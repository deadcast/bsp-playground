﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BSPTree : MonoBehaviour
{
	public Transform[] IncludedMeshes;

	const float PLANE_THICKNESS_EPSILON = 0.0001f;
	const int POINT_IN_FRONT_OF_PLANE = 0;
	const int POINT_BEHIND_PLANE = 1;
	const int POINT_ON_PLANE = 2;
	const int POLYGON_STRADDLING_PLANE = 3;
	const int POLYGON_IN_FRONT_OF_PLANE = 4;
	const int POLYGON_BEHIND_PLANE = 5;
	const int POLYGON_COPLANAR_WITH_PLANE = 6;
	const int MAX_VERTS = 1000;
	const int MAX_DEPTH = 20;
	const int MIN_LEAF_SIZE = 3;

	class Polygon
	{
		public Vector3[] Points { get; set; }
		public string Name { get; set; }
	}

	class Plane
	{
		public Vector3 Normal { get; set; }
		public float Distance { get; set; }
		public string Name { get; set; }
	}

	class BSPNode
	{
		public List<Polygon> Polygons { get; private set; }
		public BSPNode[] Child { get; private set; }

		public BSPNode(List<Polygon> polygons)
		{
			Polygons = polygons;
		}

		public BSPNode(BSPNode front, BSPNode back)
		{
			Child = new BSPNode[2] { front, back };
		}
	}

	void Start ()
	{
		var polygons = GetScenePolygons ().ToList ();
		var tree = BuildBSPTree (polygons, 0);
		RenderBSP (tree);
	}

	void RenderBSP(BSPNode node)
	{
		if (node.Child != null && node.Child[0] != null) RenderBSP (node.Child [0]);
		CreateNewMesh (node.Polygons ?? new List<Polygon>());
		if (node.Child != null && node.Child[1] != null) RenderBSP (node.Child [1]);
	}

	void CreateNewMesh(List<Polygon> polygons)
	{
		foreach (var polygon in polygons)
		{
			for (int i = 0; i < polygon.Points.Length / 3; i++)
			{
				var go = new GameObject (polygon.Name);
				var filter = go.AddComponent<MeshFilter> ();
				filter.mesh.SetVertices (polygon.Points.Skip(3 * i).Take(3).ToList());
				filter.mesh.SetTriangles (new int[] { 0, 1, 2 }, 0);
				var renderer = go.AddComponent<MeshRenderer> ();
				renderer.material = new Material (Shader.Find ("Diffuse"));
			}
		}
	}

	IEnumerable<Polygon> GetScenePolygons()
	{
		foreach (var mesh in IncludedMeshes)
		{
			var filter = mesh.GetComponent<MeshFilter> ();
			for (var i = 0; i < filter.mesh.triangles.Length / 3; i++)
			{
				yield return new Polygon
				{
					Name = mesh.name,
					Points = new Vector3[]
					{
						mesh.transform.TransformPoint(filter.mesh.vertices[filter.mesh.triangles[i * 3 + 0]]),
						mesh.transform.TransformPoint(filter.mesh.vertices[filter.mesh.triangles[i * 3 + 1]]),
						mesh.transform.TransformPoint(filter.mesh.vertices[filter.mesh.triangles[i * 3 + 2]])
					}
				};
			}
			mesh.GetComponent<MeshRenderer> ().enabled = false;
		}
	}

	Plane PickSplittingPlane(List<Polygon> polygons)
	{
		// Blend factor for optimizing for balance or splits (should be tweaked)
		const float K = 0.8f;
		// Variables for tracking best splitting plane seen so far
		Plane bestPlane = new Plane();
		float bestScore = float.MaxValue;

		// Try the plane of each polygon as a dividing plane
		for (int i = 0; i < polygons.Count; i++)
		{
			int numInFront = 0, numBehind = 0, numStraddling = 0;
			Plane plane = GetPlaneFromPolygon(polygons[i]);
			// Test against all other polygons
			for (int j = 0; j < polygons.Count; j++)
			{
				// Ignore testing against self
				if (i == j) continue;
				// Keep standing count of the various poly-plane relationships
				switch (ClassifyPolygonToPlane(polygons[j], plane))
				{
				case POLYGON_COPLANAR_WITH_PLANE:
					/* Coplanar polygons treated as being in front of plane */
				case POLYGON_IN_FRONT_OF_PLANE:
					numInFront++;
					break;
				case POLYGON_BEHIND_PLANE:
					numBehind++;
					break;
				case POLYGON_STRADDLING_PLANE:
					numStraddling++;
					break;
				}
			}
			// Compute score as a weighted combination (based on K, with K in range
			// 0..1) between balance and splits (lower score is better)
			float score = K * numStraddling + (1.0f - K) * Mathf.Abs(numInFront - numBehind);
			if (score < bestScore)
			{
				bestScore = score;
				bestPlane = plane;
			}
		}
		return bestPlane;
	}

	Plane GetPlaneFromPolygon (Polygon polygon)
	{
		var edgeA = polygon.Points [0] - polygon.Points [1];
		var edgeB = polygon.Points [1] - polygon.Points [2];
		var planeNormal = Vector3.Cross (edgeA, edgeB).normalized;
		var distance = Vector3.Dot (planeNormal, polygon.Points [0]);
		return new Plane { Name = polygon.Name, Normal = planeNormal, Distance = distance };
	}

	int ClassifyPolygonToPlane(Polygon polygon, Plane plane)
	{
		// Loop over all polygon vertices and count how many vertices
		// lie in front of and how many lie behind of the thickened plane
		int numInFront = 0, numBehind = 0;
		//int numVerts = poly->NumVertices();
		for (int i = 0; i < polygon.Points.Length; i++)
		{
			//Point p = poly->GetVertex(i);
			Vector3 p = polygon.Points[i];
			switch (ClassifyPointToPlane(p, plane))
			{
			case POINT_IN_FRONT_OF_PLANE:
				numInFront++;
				break;
			case POINT_BEHIND_PLANE:
				numBehind++;
				break;
			}
		}
		// If vertices on both sides of the plane, the polygon is straddling
		if (numBehind != 0 && numInFront != 0)
			return POLYGON_STRADDLING_PLANE;
		// If one or more vertices in front of the plane and no vertices behind
		// the plane, the polygon lies in front of the plane
		if (numInFront != 0) 
			return POLYGON_IN_FRONT_OF_PLANE;
		// Ditto, the polygon lies behind the plane if no vertices in front of
		// the plane, and one or more vertices behind the plane
		if (numBehind != 0) 
			return POLYGON_BEHIND_PLANE;
		// All vertices lie on the plane so the polygon is coplanar with the plane
		return POLYGON_COPLANAR_WITH_PLANE;
	}

	int ClassifyPointToPlane(Vector3 p, Plane plane)
	{
		// Compute signed distance of point from plane
		float dist = Vector3.Dot(plane.Normal, p) - plane.Distance;
		// Classify p based on the signed distance
		if (dist > PLANE_THICKNESS_EPSILON)
			return POINT_IN_FRONT_OF_PLANE;
		if (dist < -PLANE_THICKNESS_EPSILON)
			return POINT_BEHIND_PLANE;
		return POINT_ON_PLANE;
	}

	Vector3 IntersectEdgeAgainstPlane(Vector3 pointA, Vector3 pointB, Plane plane)
	{
		var edge = pointB - pointA;
		var dna = plane.Distance - Vector3.Dot (plane.Normal, pointA);
		var nba = Vector3.Dot (plane.Normal, edge);
		return pointA + ((dna / nba) * edge);
	}

	void SplitPolygon(Polygon polygon, Plane plane, out Polygon frontPoly, out Polygon backPoly)
	{
		int numFront = 0, numBack = 0;
		var frontVerts = new Vector3[MAX_VERTS];
		var backVerts = new Vector3[MAX_VERTS];

		// Test all edges (a, b) starting with edge from last to first vertex
		int numVerts = polygon.Points.Length;
		Vector3 a = polygon.Points[numVerts - 1];
		int aSide = ClassifyPointToPlane(a, plane);

		// Loop over all edges given by vertex pair (n-1, n)
		for (int n = 0; n < numVerts; n++)
		{
			Vector3 b = polygon.Points[n];
			int bSide = ClassifyPointToPlane(b, plane);
			if (bSide == POINT_IN_FRONT_OF_PLANE)
			{
				if (aSide == POINT_BEHIND_PLANE)
				{
					// Edge (a, b) straddles, output intersection point to both sides
					Vector3 i = IntersectEdgeAgainstPlane(a, b, plane);
					Debug.Assert(ClassifyPointToPlane(i, plane) == POINT_ON_PLANE);
					frontVerts[numFront++] = backVerts[numBack++] = i;
				}
				// In all three cases, output b to the front side
				frontVerts[numFront++] = b;
			}
			else if (bSide == POINT_BEHIND_PLANE)
			{
				if (aSide == POINT_IN_FRONT_OF_PLANE)
				{
					// Edge (a, b) straddles plane, output intersection point
					Vector3 i = IntersectEdgeAgainstPlane(a, b, plane);
					Debug.Assert(ClassifyPointToPlane(i, plane) == POINT_ON_PLANE);
					frontVerts[numFront++] = backVerts[numBack++] = i;
				} 
				else if (aSide == POINT_ON_PLANE)
				{
					// Output a when edge (a, b) goes from ‘on’ to ‘behind’ plane
					backVerts[numBack++] = a;
				}
				// In all three cases, output b to the back side
				backVerts[numBack++] = b;
			} 
			else
			{
				// b is on the plane. In all three cases output b to the front side
				frontVerts[numFront++] = b;
				// In one case, also output b to back side
				if (aSide == POINT_BEHIND_PLANE) backVerts[numBack++] = b;
			}
			// Keep b as the starting point of the next edge
			a = b;
			aSide = bSide;
		}

		var front = frontVerts.Where (e => e != Vector3.zero).ToArray();
		front = Triangulate (front, front.Length);

		var back = backVerts.Where (e => e != Vector3.zero).ToArray ();
		back = Triangulate (back, back.Length);

		// Create (and return) two new polygons from the two vertex lists
		frontPoly = new Polygon { Points = front.ToArray() };
		backPoly = new Polygon { Points = back.ToArray() };
	}

	BSPNode BuildBSPTree(List<Polygon> polygons, int depth)
	{
		// Return NULL tree if there are no polygons
		if (!polygons.Any ())
			return null;

		// Get number of polygons in the input vector
		int numPolygons = polygons.Count;

		// If criterion for a leaf is matched, create a leaf node from remaining polygons
		if (depth >= MAX_DEPTH || numPolygons <= MIN_LEAF_SIZE)
			return new BSPNode(polygons);

		// Select best possible partitioning plane based on the input geometry
		Plane splitPlane = PickSplittingPlane(polygons);

		var frontList = new List<Polygon> ();
		var backList = new List<Polygon>();

		// Test each polygon against the dividing plane, adding them
		// to the front list, back list, or both, as appropriate
		for (int i = 0; i < numPolygons; i++) {
			Polygon poly = polygons[i], frontPart, backPart;
			switch (ClassifyPolygonToPlane(poly, splitPlane)) {
			case POLYGON_COPLANAR_WITH_PLANE:
				// What's done in this case depends on what type of tree is being
				// built. For a node-storing tree, the polygon is stored inside
				// the node at this level (along with all other polygons coplanar
				// with the plane). Here, for a leaf-storing tree, coplanar polygons
				// are sent to either side of the plane. In this case, to the front
				// side, by falling through to the next case
			case POLYGON_IN_FRONT_OF_PLANE:
				frontList.Add(poly);
				break;
			case POLYGON_BEHIND_PLANE:
				backList.Add(poly);
				break;
			case POLYGON_STRADDLING_PLANE:
				// Split polygon to plane and send a part to each side of the plane
				SplitPolygon(poly, splitPlane, out frontPart, out backPart);
				frontList.Add(frontPart);
				backList.Add(backPart);
				break;
			}
		}

		// Recursively build child subtrees and return new tree root combining them
		BSPNode frontTree = BuildBSPTree(frontList, depth + 1);
		BSPNode backTree = BuildBSPTree(backList, depth + 1);
		return new BSPNode(frontTree, backTree);
	}

	bool IsTriangleCCW(Vector3 a, Vector3 b, Vector3 c)
	{
		float winding = (a.x * b.y - b.x * a.y) + (b.x * c.y - c.x * b.y) + (c.x * a.y - a.x * c.y);
		return winding > 0 ? false : true;
	}

	bool IsPointInTriangle(Vector3 p, Vector3 a, Vector3 b, Vector3 c)
	{
		float u, v, w;
		Barycentric(a, b, c, p, out u, out v,  out w);
		return v >= 0.0f && w >= 0.0f && (v + w) <= 1.0f;
	}

	void Barycentric(Vector3 a, Vector3 b, Vector3 c, Vector3 p, out float u, out float v, out float w)
	{
		Vector3 v0 = b - a, v1 = c - a, v2 = p - a;
		float d00 = Vector3.Dot(v0, v0);
		float d01 = Vector3.Dot(v0, v1);
		float d11 = Vector3.Dot(v1, v1);
		float d20 = Vector3.Dot(v2, v0);
		float d21 = Vector3.Dot(v2, v1);
		float denom = d00 * d11 - d01 * d01;
		v = (d11 * d20 - d01 * d21) / denom;
		w = (d00 * d21 - d01 * d20) / denom;
		u = 1.0f - v - w;
	}

	Vector3[] Triangulate(Vector3[] v, int n)
	{
		List<Vector3> newPoints = new List<Vector3> ();
		// Set up previous and next links to effectively form a double-linked vertex list
		int[] prev = new int[MAX_VERTS], next = new int[MAX_VERTS];
		for (int i = 0; i < n; i++)
		{
			prev[i] = i - 1;
			next[i] = i + 1;
		}
		prev[0] = n - 1;
		next[n - 1] = 0;

		// Start at vertex 0
		int j = 0;
		// Keep removing vertices until just a triangle left
		while (n > 3) 
		{
			// Test if current vertex, v[i], is an ear
			bool isEar = true;
			// An ear must be convex (here counterclockwise)
			if (IsTriangleCCW(v[prev[j]], v[j], v[next[j]])) 
			{
				// Loop over all vertices not part of the tentative ear
				int k = next[next[j]];
				do 
				{
					// If vertex k is inside the ear triangle, then this is not an ear
					if (IsPointInTriangle(v[k], v[prev[j]], v[j], v[next[j]])) 
					{
						isEar = false;
						break;
					}
					k = next[k];
				} 
				while (k != prev[j]);
			} 
			else 
			{
				// The ‘ear’ triangle is clockwise so v[i] is not an ear
				isEar = false;
			}

			// If current vertex v[i] is an ear, delete it and visit the previous vertex
			if (isEar) 
			{
				// Triangle (v[i], v[prev[i]], v[next[i]]) is an ear
					//...output triangle here...
					// ‘Delete’ vertex v[i] by redirecting next and previous links
					// of neighboring verts past it. Decrement vertex count
				newPoints.Add (v [j]);
				newPoints.Add (v [next [j]]);
				newPoints.Add (v [prev [j]]);

				next[prev[j]] = next[j];
				prev[next[j]] = prev[j];
				n--;
				// Visit the previous vertex next
				j = prev[j];
			} 
			else 
			{
				// Current vertex is not an ear; visit the next vertex
				j = next[j];
			}
		}

		// Last tri
		newPoints.Add (v [j]);
		newPoints.Add (v [next [j]]);
		newPoints.Add (v [prev [j]]);

		return newPoints.ToArray ();
	}
}
